import os
import random


def set_key(file_path: str, key: str, value: str):
	text = open(file_path, 'r').read()
	text = text.replace(f'||{key}||', value)
	open(file_path, 'w').write(text)


def main():
	chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
	django_secret_key = ''.join(random.choice(chars) for i in range(50))

	settings_file_path = os.path.join('source', 'settings', 'base.py')
	set_key(settings_file_path, 'SECRET_KEY', django_secret_key)


if __name__ == "__main__":
	main()
