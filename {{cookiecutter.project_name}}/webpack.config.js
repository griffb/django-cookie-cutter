const {merge} = require("webpack-merge");

const common = require("./webpack.common");
const path = require("path");

function buildConfig(appName) {
    let basePath = path.join(__dirname, "source", "apps", appName, "static", appName);
    let entryPath = path.join(basePath, appName + "_app.js");
    let outputPath = path.join(basePath, "vue", "bundles");

    return merge(common, {
        name: appName,
        entry: entryPath,
        output: {
            path: outputPath,
            filename: appName + '.[name].js',
            chunkFilename: appName + '.[chunkhash].js',
        },
        resolve: {
            alias: {
                '@': basePath,
            }
        }
    });
}


module.exports = (env, argv) => [
    merge(buildConfig("dashboard"), {mode: env.mode}),
    merge(buildConfig("user"), {mode: env.mode}),
    merge(buildConfig("frontend"), {mode: env.mode}),
];
