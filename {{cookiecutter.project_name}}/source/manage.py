#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

MANAGE_ROOT = os.path.dirname(os.path.realpath(__file__))
APPS_ROOT = os.path.join(MANAGE_ROOT, 'apps')
SETTINGS_ROOT = os.path.join(MANAGE_ROOT, 'settings')

sys.path.append(MANAGE_ROOT)
sys.path.append(APPS_ROOT)
sys.path.append(SETTINGS_ROOT)


def main():
	"""Run administrative tasks."""
	try:
		from django.core.management import execute_from_command_line
	except ImportError as exc:
		raise ImportError(
			"Couldn't import Django. Are you sure it's installed and "
			"available on your PYTHONPATH environment variable? Did you "
			"forget to activate a virtual environment?"
		) from exc
	execute_from_command_line(sys.argv)


if __name__ == '__main__':
	main()
