from base import *

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql',
		'NAME': 'bitbucket',
		'USER': 'bitbucket',
		'PASSWORD': 'bitbucket',
		'HOST': '127.0.0.1',
		'PORT': '5432',
	},
}
