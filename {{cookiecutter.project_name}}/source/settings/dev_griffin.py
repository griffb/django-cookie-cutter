from dev_base import *

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql',
		'NAME': '{{ cookiecutter.project_name.lower()|replace(" ", "-")|replace(".", "-")|replace("_", "-") }}',
		'USER': 'postgres',
		'PASSWORD': 'postgres',
		'HOST': '127.0.0.1',
		'PORT': '5432',
	},
}

DATABASES['default'] = DATABASES['default']

BROKER_URL = 'redis://127.0.0.1:6379'
CELERY_RESULT_BACKEND = 'redis://127.0.0.1:6379'
