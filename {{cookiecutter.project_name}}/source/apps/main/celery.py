import os
import sys

from celery import Celery

MANAGE_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.insert(0, MANAGE_ROOT)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.dev_griffin')

app = Celery('main')
app.config_from_envvar('DJANGO_SETTINGS_MODULE')
app.autodiscover_tasks()
