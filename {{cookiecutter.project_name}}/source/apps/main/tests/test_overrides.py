from unittest.mock import MagicMock

from django.contrib.auth import authenticate

from main.overrides import OverrideTestCase


class TestEmailBackend(OverrideTestCase):
	def setUp(self) -> None:
		self.setup_test_user()

	def test_user_with_email(self):
		mock_request = MagicMock()
		user = authenticate(mock_request, email=self.test_user.email, password='password')
		self.assertIsNotNone(user)

	def test_user_with_email_as_username(self):
		mock_request = MagicMock()
		user = authenticate(mock_request, username=self.test_user.email, password='password')
		self.assertIsNotNone(user)

	def test_user_without_email_or_username(self):
		mock_request = MagicMock()
		with self.assertRaises(Exception) as exc:
			user = authenticate(mock_request, password='password')
			self.assertIsNone(user)
			self.assertEqual(exc.exception, 'Failed to login user, no username or email supplied.')

	def test_wrong_email(self):
		mock_request = MagicMock()
		user = authenticate(mock_request, email='nonexist@mail.com', password='password')
		self.assertIsNone(user)

	def test_wrong_password(self):
		mock_request = MagicMock()
		user = authenticate(mock_request, email=self.test_user.email, password='password1')
		self.assertIsNone(user)
