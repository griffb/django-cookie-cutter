from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.mixins import LoginRequiredMixin
from django.test import TestCase
from django.views import View

from user.models import User


class EmailBackend(ModelBackend):
	def authenticate(self, request, email=None, password=None, **kwargs):
		user_model = get_user_model()
		username = kwargs.get('username')

		try:
			if email:
				user = user_model.objects.get(email=email)
			elif username:
				user = user_model.objects.get(email=username)
			else:
				raise Exception('Failed to login user, no username or email supplied.')

			if user.check_password(password):
				return user

		except user_model.DoesNotExist:
			return None


# @override_settings(SOMETHING=False)
class OverrideTestCase(TestCase):
	# fixtures = ["test_defaults.json"]
	maxDiff = None

	def setup_test_user(self):
		self.test_user = User.objects.create_user(
			email='test@user.mail',
			password='password',
			first_name='First',
			last_name='Last'
		)

	def tearDown(self) -> None:
		from django.core.cache import cache
		cache.clear()
		super().tearDown()


class LoginRequiredView(LoginRequiredMixin, View):
	pass
