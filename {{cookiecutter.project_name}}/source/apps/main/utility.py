import logging

from django.core.mail import send_mail
from django.utils.translation import gettext


def create_error_list(errors: dict) -> dict:
	return {gettext('errors'): errors}


def create_missing_key_error(key: str) -> dict:
	errors = {key: f'Missing key "{key}"'}
	return create_error_list(errors)


def chunker(lst, size):
	for i in range(0, len(lst), size):
		yield lst[i:i + size]


def send_email(subject, message, recipients):
	if isinstance(recipients, str):
		recipients = [recipients]

	send_mail(f'VitalMonitor: {subject}', message, 'griffin.test.gmb@gmail.com', recipients)


def create_logger(app_name=None):
	new_logger = logging.getLogger(app_name or __name__)
	new_logger.setLevel(logging.INFO)
	log_format = '[%(asctime)-15s] [%(levelname)s] %(funcName)s(): %(message)s'
	logging.basicConfig(format=log_format)
	return new_logger
