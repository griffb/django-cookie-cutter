import user.views as views
from django.urls import path

app_name = 'user'
urlpatterns = [
	path('', views.index, name='index'),
	path('register/', views.RegisterView.as_view(), name='register'),
	path('login/', views.LoginView.as_view(), name='login'),
	path('logout/', views.logout_view, name='logout'),
	path('api/changepassword/', views.PasswordChangeView.as_view(), name='changepassword'),
]
