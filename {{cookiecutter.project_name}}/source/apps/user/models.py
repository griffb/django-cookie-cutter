from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext

from user.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
	created_ts = models.DateTimeField(auto_now_add=True, verbose_name=gettext('Created Timestamp'))
	updated_ts = models.DateTimeField(auto_now=True, verbose_name=gettext('Created Timestamp'))
	email = models.EmailField(blank=False, null=False, verbose_name=gettext('Email Address'), unique=True)
	first_name = models.TextField(blank=False, null=False, verbose_name=gettext('First Name'))
	last_name = models.TextField(blank=False, null=False, verbose_name=gettext('Last Name'))
	is_staff = models.BooleanField(blank=False, null=False, default=False, verbose_name=gettext('Is Staff'))
	is_active = models.BooleanField(blank=False, null=False, default=True, verbose_name=gettext('Is Active'))

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	objects = UserManager()

	def __str__(self):
		return self.email
