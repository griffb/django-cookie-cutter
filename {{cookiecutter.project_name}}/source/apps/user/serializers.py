from django.utils.translation import gettext
from rest_framework import serializers

from user.models import User


class LoginSerializer(serializers.Serializer):
	email = serializers.EmailField(allow_null=False, allow_blank=False)
	password = serializers.CharField(allow_null=False, allow_blank=False)


class RegistrationSerializer(serializers.Serializer):
	email = serializers.EmailField(allow_null=False, allow_blank=False)
	first_name = serializers.CharField(allow_null=False, allow_blank=False)
	last_name = serializers.CharField(allow_null=False, allow_blank=False)
	password_one = serializers.CharField(allow_null=False, allow_blank=False)
	password_two = serializers.CharField(allow_null=False, allow_blank=False)

	def validate_email(self, value):
		if User.objects.filter(email=value).exists():
			raise serializers.ValidationError(gettext('Email already in use.'))
		return value

	def validate(self, attrs):
		password_one = attrs['password_one']
		password_two = attrs['password_two']

		if password_one != password_two:
			raise serializers.ValidationError({
				'password_one': gettext('Passwords must match.'),
				'password_two': gettext('Passwords must match.'),
			})

		return attrs

	def create(self, validated_data) -> User:
		email = validated_data['email']
		password_one = validated_data['password_one']
		first_name = validated_data['first_name']
		last_name = validated_data['last_name']

		user = User.objects.create_user(
			email=email,
			password=password_one,
			first_name=first_name,
			last_name=last_name,
		)
		return user


class ChangePasswordSerializer(serializers.Serializer):
	current_password = serializers.CharField(allow_null=False, allow_blank=False)
	new_password_one = serializers.CharField(allow_null=False, allow_blank=False)
	new_password_two = serializers.CharField(allow_null=False, allow_blank=False)

	def validate(self, attrs):
		new_password_one = attrs['new_password_one']
		new_password_two = attrs['new_password_two']

		if new_password_one != new_password_two:
			raise serializers.ValidationError({
				'new_password_one': gettext('Passwords must match.'),
				'new_password_two': gettext('Passwords must match.'),
			})

		return attrs
