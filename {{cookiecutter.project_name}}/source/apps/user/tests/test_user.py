from main.overrides import OverrideTestCase
from user.models import User


class UsersManagersTests(OverrideTestCase):

	def test_create_user(self):
		created_user = User.objects.create_user(email='normal@user.com', password='foo')

		self.assertEqual(created_user.email, 'normal@user.com')
		self.assertTrue(created_user.is_active)
		self.assertFalse(created_user.is_staff)
		self.assertFalse(created_user.is_superuser)

		with self.assertRaises(TypeError):
			User.objects.create_user()

		with self.assertRaises(TypeError):
			User.objects.create_user(email='')

		with self.assertRaises(ValueError):
			User.objects.create_user(email='', password="foo")

	def test_create_superuser(self):
		admin_user = User.objects.create_superuser(email='super@user.com', password='foo')

		self.assertEqual(admin_user.email, 'super@user.com')
		self.assertTrue(admin_user.is_active)
		self.assertTrue(admin_user.is_staff)
		self.assertTrue(admin_user.is_superuser)

		with self.assertRaises(ValueError):
			User.objects.create_superuser(email='super@user.com', password='foo', is_superuser=False)

		with self.assertRaises(ValueError):
			User.objects.create_superuser(email='super@user.com', password='foo', is_staff=False)


class TestUser(OverrideTestCase):

	def setUp(self) -> None:
		self.setup_test_user()

	def test_str(self):
		self.assertEqual(self.test_user.email, self.test_user.__str__())
