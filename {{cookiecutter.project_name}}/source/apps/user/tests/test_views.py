from unittest.mock import patch

from django.conf import settings
from django.urls import reverse

from main.overrides import OverrideTestCase
from user.models import User


class TestIndex(OverrideTestCase):
	def setUp(self) -> None:
		self.setup_test_user()

	@patch('django.shortcuts.loader')
	def test_unauthenticated(self, mock_loader):
		mock_loader.return_value = 'test'
		response = self.client.post(reverse('user:index'))
		self.assertEqual(response.status_code, 200)

	def test_authenticated(self):
		self.client.force_login(self.test_user)
		response = self.client.post(reverse('user:index'))
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response.url, settings.LOGGED_IN_URL)


class TestLogout(OverrideTestCase):
	def setUp(self) -> None:
		self.setup_test_user()
		self.client.force_login(self.test_user)

	def test_logout(self):
		response = self.client.post(reverse('user:logout'))
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response.url, settings.LOGOUT_URL)
		self.assertIsNone(response.wsgi_request.session.session_key)


class TestRegisterView(OverrideTestCase):
	def setUp(self) -> None:
		self.data = {
			'email': 'test@mail.com',
			'first_name': 'First',
			'last_name': 'Last',
			'password_one': 'password',
			'password_two': 'password',
		}

	def test_post(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 200)

		user = User.objects.get(email='test@mail.com')
		self.assertIsNotNone(user)
		self.assertIsNotNone(response.wsgi_request.session.session_key)
		self.assertEqual(user.first_name, 'First')
		self.assertEqual(user.last_name, 'Last')

	def test_missing_email(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		del self.data['email']
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('email', response.data['errors'])
		self.assertEqual(response.data['errors']['email'][0], 'This field is required.')
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_password_one(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		del self.data['password_one']
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('password_one', response.data['errors'])
		self.assertEqual(response.data['errors']['password_one'][0], 'This field is required.')
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_password_two(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		del self.data['password_two']
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('password_two', response.data['errors'])
		self.assertEqual(response.data['errors']['password_two'][0], 'This field is required.')
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_first_name(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		del self.data['first_name']
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('first_name', response.data['errors'])
		self.assertEqual(response.data['errors']['first_name'][0], 'This field is required.')
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_last_name(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		del self.data['last_name']
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('last_name', response.data['errors'])
		self.assertEqual(response.data['errors']['last_name'][0], 'This field is required.')
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_email_already_in_use(self):
		User.objects.create_user(
			email='test@mail.com',
			password='password',
			first_name='First',
			last_name='Last'
		)
		self.assertTrue(User.objects.filter(email='test@mail.com').exists())

		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('email', response.data['errors'])
		self.assertEqual(response.data['errors']['email'][0], 'Email already in use.')

	def test_non_matching_passwords(self):
		self.assertFalse(User.objects.filter(email='test@mail.com').exists())

		self.data['password_two'] = 'password_different'
		response = self.client.post(reverse('user:register'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('password_one', response.data['errors'])
		self.assertIn('password_two', response.data['errors'])
		self.assertEqual(response.data['errors']['password_one'][0], 'Passwords must match.')
		self.assertEqual(response.data['errors']['password_two'][0], 'Passwords must match.')


class TestLoginView(OverrideTestCase):
	def setUp(self) -> None:
		self.data = {'email': 'test@mail.com', 'password': 'password'}
		self.user = User.objects.create_user(**self.data)

	def test_post(self):
		response = self.client.post(reverse('user:login'), self.data)
		self.assertEqual(response.status_code, 200)

		self.assertIsNotNone(response.wsgi_request.session.session_key)

	def test_missing_email(self):
		del self.data['email']
		response = self.client.post(reverse('user:login'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('email', response.data['errors'])
		self.assertEqual(response.data['errors']['email'][0], 'This field is required.')
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_password(self):
		del self.data['password']
		response = self.client.post(reverse('user:login'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('password', response.data['errors'])
		self.assertEqual(response.data['errors']['password'][0], 'This field is required.')
		self.assertIsNone(response.wsgi_request.session.session_key)

	def test_missing_password_incorrect(self):
		self.data['password'] = 'wrongpassword'

		response = self.client.post(reverse('user:login'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('email', response.data['errors'])
		self.assertIn('password', response.data['errors'])
		self.assertEqual(response.data['errors']['password'], 'Incorrect email or password.')
		self.assertEqual(response.data['errors']['email'], [])
		self.assertIsNone(response.wsgi_request.session.session_key)


class TestPasswordChangeView(OverrideTestCase):
	def setUp(self) -> None:
		self.data = {
			'current_password': 'password',
			'new_password_one': 'newpassword',
			'new_password_two': 'newpassword',
		}
		self.user = User.objects.create_user(email='test@mail.com', password='password')
		self.client.force_login(self.user)

	def test_post(self):
		self.assertTrue(self.user.check_password('password'))

		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 200)

		self.user.refresh_from_db()
		self.assertTrue(self.user.check_password('newpassword'))

	def test_missing_current_password(self):
		self.assertTrue(self.user.check_password('password'))

		del self.data['current_password']
		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('current_password', response.data['errors'])
		self.assertEqual(response.data['errors']['current_password'][0], 'This field is required.')

		self.user.refresh_from_db()
		self.assertTrue(self.user.check_password('password'))

	def test_missing_new_password_one(self):
		self.assertTrue(self.user.check_password('password'))

		del self.data['new_password_one']
		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('new_password_one', response.data['errors'])
		self.assertEqual(response.data['errors']['new_password_one'][0], 'This field is required.')

		self.user.refresh_from_db()
		self.assertTrue(self.user.check_password('password'))

	def test_missing_new_password_two(self):
		self.assertTrue(self.user.check_password('password'))

		del self.data['new_password_two']
		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('new_password_two', response.data['errors'])
		self.assertEqual(response.data['errors']['new_password_two'][0], 'This field is required.')

		self.user.refresh_from_db()
		self.assertTrue(self.user.check_password('password'))

	def test_missing_password_incorrect(self):
		self.assertTrue(self.user.check_password('password'))
		self.data['current_password'] = 'wrongpassword'

		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('current_password', response.data['errors'])
		self.assertEqual(response.data['errors']['current_password'], 'Incorrect password.')
		self.assertTrue(self.user.check_password('password'))

	def test_non_matching_passwords(self):
		self.data['new_password_two'] = 'password_different'
		response = self.client.post(reverse('user:changepassword'), self.data)
		self.assertEqual(response.status_code, 400)

		self.assertIn('new_password_one', response.data['errors'])
		self.assertIn('new_password_two', response.data['errors'])
		self.assertEqual(response.data['errors']['new_password_one'][0], 'Passwords must match.')
		self.assertEqual(response.data['errors']['new_password_two'][0], 'Passwords must match.')
