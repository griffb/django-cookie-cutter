import main.utility as utility
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils.translation import gettext
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from user.serializers import RegistrationSerializer, LoginSerializer, ChangePasswordSerializer


def index(request):
	# todo: change the redirect URL
	if request.user.is_authenticated:
		return HttpResponseRedirect(settings.LOGGED_IN_URL)
	return render(request, 'user/index.html')


def logout_view(request):
	logout(request)
	return redirect(settings.LOGOUT_URL)


class RegisterView(APIView):
	def post(self, request):
		serializer = RegistrationSerializer(data=request.data)
		if serializer.is_valid():
			user = serializer.save()
			login(request, user)
			return Response(status=status.HTTP_200_OK)

		errors = utility.create_error_list(serializer.errors)
		return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(APIView):
	def post(self, request):
		serializer = LoginSerializer(data=request.data)
		if not serializer.is_valid():
			errors = utility.create_error_list(serializer.errors)
			return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)

		user = authenticate(request, email=serializer.data['email'], password=serializer.data['password'])
		if not user:
			errors = utility.create_error_list({
				'email': [],
				'password': gettext('Incorrect email or password.')
			})
			return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)

		login(request, user)
		return Response(status=status.HTTP_200_OK)


class PasswordChangeView(APIView):
	authentication_classes = [SessionAuthentication]
	permission_classes = [IsAuthenticated]

	def post(self, request):
		serializer = ChangePasswordSerializer(data=request.data)
		if not serializer.is_valid():
			errors = utility.create_error_list(serializer.errors)
			return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)

		if not request.user.check_password(serializer.data['current_password']):
			errors = utility.create_error_list({
				'current_password': gettext('Incorrect password.')
			})
			return Response(data=errors, status=status.HTTP_400_BAD_REQUEST)

		request.user.set_password(serializer.data['new_password_one'])
		request.user.save()
		return Response(status=status.HTTP_200_OK)
